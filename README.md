# OpenML dataset: SignMNIST

https://www.openml.org/d/45082

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

The original MNIST image dataset of handwritten digits is a popular benchmark for image-based machine learning methods but researchers have renewed efforts to update it and develop drop-in replacements that are more challenging for computer vision and original for real-world applications.
The dataset format is patterned to match closely with the classic MNIST. Each training and test case represents a label (0-25) as a one-to-one map for each alphabetic letter A-Z (and no cases for 9=J or 25=Z because of gesture motions). The training data (27,455 cases) and test data (7172 cases) are approximately half the size of the standard MNIST but otherwise similar with a header row of label, pixel1, pixel2 ... pixel784 which represent a single 28x28 pixel image with grayscale values between 0-255.
The train and test data have been concatenated and can be retrieved by selecting the first 27,455 rows for train and the last 7172 for test.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45082) of an [OpenML dataset](https://www.openml.org/d/45082). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45082/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45082/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45082/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

